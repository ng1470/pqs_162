package addressbook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit Tests for Contact.java with Code Coverage of 100%.
 * 
 * @author Nishma Gorwara
 */
public class ContactTest {
  private Contact contactTest;
  private Contact contactNamePhone;
  private Contact contactEmailNoteAddress;

  @Before
  public void setUp() {
    contactTest = new Contact.Builder().withName("Test Name").withPhoneNumber("1234567890")
        .withEmail("test@test.com")
        .withAddress("Test Street, Apt Test, Test City, Test State, 12345").withNote("Test Note")
        .build();
    // Contact with only Name and Phone Number
    contactNamePhone = new Contact.Builder().withName("Test Name 1").withPhoneNumber("1234567890")
        .build();
    // Contact with only Email, Address and Note
    contactEmailNoteAddress = new Contact.Builder().withEmail("test2@test.com")
        .withAddress("Test2 Street, Apt Test2, Test2 City, Test2 State, 12345")
        .withNote("Test Note 2").build();
  }

  @Test
  public void getNameTest() {
    Contact contactName = new Contact.Builder().withName("Test Name").build();
    assertEquals("Test Name", contactName.getName());
  }

  @Test
  public void getNameTest_Empty() {
    Contact contactName = new Contact.Builder().withName("").build();
    assertEquals("", contactName.getName());
  }

  @Test
  public void getNameTest_Null() {
    Contact contactName = new Contact.Builder().withName(null).build();
    assertEquals(null, contactName.getName());
  }

  @Test(expected = NullPointerException.class)
  public void getNameTest_NotPresent() {
    Contact contactPhoneNumber = new Contact.Builder().withPhoneNumber("1234567890").build();
    contactPhoneNumber.getName();
  }

  @Test
  public void getEmailAddressTest() {
    Contact contactEmailAddress = new Contact.Builder().withEmail("test@test.com").build();
    assertEquals("test@test.com", contactEmailAddress.getEmailAddress());
  }

  @Test
  public void getEmailAddressTest_Empty() {
    Contact contactEmailAddress = new Contact.Builder().withEmail("").build();
    assertEquals("", contactEmailAddress.getEmailAddress());
  }

  @Test
  public void getEmailAddressTest_Null() {
    Contact contactEmailAddress = new Contact.Builder().withEmail(null).build();
    assertEquals(null, contactEmailAddress.getEmailAddress());
  }

  @Test(expected = NullPointerException.class)
  public void getEmailAddressTest_NotPresent() {
    Contact contactPhoneNumber = new Contact.Builder().withPhoneNumber("1234567890").build();
    contactPhoneNumber.getEmailAddress();
  }

  @Test
  public void getPhoneNumberTest() {
    Contact contactPhoneNumber = new Contact.Builder().withPhoneNumber("1111111111").build();
    assertEquals("1111111111", contactPhoneNumber.getPhoneNumber());
  }

  @Test
  public void getPhoneNumberTest_Empty() {
    Contact contactPhoneNumber = new Contact.Builder().withPhoneNumber("").build();
    assertEquals("", contactPhoneNumber.getPhoneNumber());
  }

  @Test
  public void getPhoneNumberTest_Null() {
    Contact contactPhoneNumber = new Contact.Builder().withPhoneNumber(null).build();
    assertEquals(null, contactPhoneNumber.getPhoneNumber());
  }

  @Test(expected = NullPointerException.class)
  public void getPhoneNumberTest_NotPresent() {
    Contact contactName = new Contact.Builder().withName("Test Name").build();
    contactName.getPhoneNumber();
  }

  @Test
  public void getAddressTest() {
    Contact contactAddress = new Contact.Builder()
        .withAddress("Test Street, Apt Test, Test City, Test State, 12345").build();
    assertEquals("Test Street, Apt Test, Test City, Test State, 12345",
        contactAddress.getAddress());
  }

  @Test
  public void getAddressTest_Empty() {
    Contact contactAddress = new Contact.Builder().withAddress("").build();
    assertEquals("", contactAddress.getAddress());
  }

  @Test
  public void getAddressTest_Null() {
    Contact contactAddress = new Contact.Builder().withAddress(null).build();
    assertEquals(null, contactAddress.getAddress());
  }

  @Test(expected = NullPointerException.class)
  public void getAddressTest_NotPresent() {
    Contact contactEmailAddress = new Contact.Builder().withEmail("test@test.com").build();
    contactEmailAddress.getAddress();
  }

  @Test
  public void getNoteTest() {
    Contact contactNote = new Contact.Builder().withNote("Test Note").build();
    assertEquals("Test Note", contactNote.getNote());
  }

  @Test
  public void getNoteTest_Empty() {
    Contact contactNote = new Contact.Builder().withNote("").build();
    assertEquals("", contactNote.getNote());
  }

  @Test
  public void getNoteTest_Null() {
    Contact contactNote = new Contact.Builder().withNote(null).build();
    assertEquals(null, contactNote.getNote());
  }

  @Test(expected = NullPointerException.class)
  public void getNoteTest_NotPresent() {
    Contact contactEmailAddress = new Contact.Builder().withEmail("test@test.com").build();
    contactEmailAddress.getNote();
  }

  @Test(expected = IllegalArgumentException.class)
  public void withNameTest_Multiple() {
    Contact contactName = new Contact.Builder().withName("Test Name 1").withName("Test Name 2")
        .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void withEmailTest_Multiple() {
    Contact contactEmailAddress = new Contact.Builder().withEmail("test1@test.com")
        .withEmail("test2@test.com").build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void withAddressTest_Multiple() {
    Contact contactAddress = new Contact.Builder().withAddress("Test 1 Street")
        .withAddress("Test 2 Building").build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void withPhoneNumberTest_Multiple() {
    Contact contactPhoneNumber = new Contact.Builder().withPhoneNumber("1234567890")
        .withPhoneNumber("0123456789").build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void withNoteTest_Multiple() {
    Contact contactNote = new Contact.Builder().withNote("Test Note 1").withNote("Test Note 2")
        .build();
  }

  @Test(expected = IllegalArgumentException.class)
  public void buildTest_NoArgument() {
    Contact contactBuild = new Contact.Builder().build();
  }

  @Test
  public void matchTest_Name() {
    Contact contactName = new Contact.Builder().withName("Test Name").build();
    String matchName = "test name";
    assertTrue(contactName.match(AddressBook.ContactAttribute.NAME, matchName));
    assertFalse(contactEmailNoteAddress.match(AddressBook.ContactAttribute.NAME, matchName));
  }

  @Test
  public void matchTest_Email() {
    Contact contactEmail = new Contact.Builder().withEmail("test@test.com").build();
    String matchEmail = "test@test.com";
    assertTrue(contactEmail.match(AddressBook.ContactAttribute.EMAIL, matchEmail));
    assertFalse(contactNamePhone.match(AddressBook.ContactAttribute.EMAIL, matchEmail));
  }

  @Test
  public void matchTest_Phone() {
    Contact contactPhone = new Contact.Builder().withPhoneNumber("1234567890").build();
    String matchPhone = "1234567890";
    assertTrue(contactPhone.match(AddressBook.ContactAttribute.PHONE, matchPhone));
    assertFalse(contactEmailNoteAddress.match(AddressBook.ContactAttribute.PHONE, matchPhone));
  }

  @Test
  public void matchTest_Address() {
    Contact contactAddress = new Contact.Builder().withAddress("Test Street").build();
    String matchAddress = "test street";
    assertTrue(contactAddress.match(AddressBook.ContactAttribute.ADDRESS, matchAddress));
    assertFalse(contactNamePhone.match(AddressBook.ContactAttribute.ADDRESS, matchAddress));
  }

  @Test
  public void matchTest_Note() {
    Contact contactNote = new Contact.Builder().withNote("Test Note").build();
    String matchNote = "TEST NOTE";
    assertTrue(contactNote.match(AddressBook.ContactAttribute.NOTE, matchNote));
    assertFalse(contactNamePhone.match(AddressBook.ContactAttribute.NOTE, matchNote));
  }

  @Test
  public void toStringTest() {
    String contactString = "Test Name\ntest@test.com\n1234567890\nTest Street, Apt Test, Test City, Test State, 12345\nTest Note\n";
    assertEquals(contactString, contactTest.toString());
  }

  @Test
  public void toStringTest_NullFields() {
    String contactString1 = "Test Name 1\n1234567890\n";
    String contactString2 = "test2@test.com\nTest2 Street, Apt Test2, Test2 City, Test2 State, 12345\nTest Note 2\n";
    assertEquals(contactString1, contactNamePhone.toString());
    assertEquals(contactString2, contactEmailNoteAddress.toString());
  }
}
