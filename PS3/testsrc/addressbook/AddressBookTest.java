package addressbook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import addressbook.AddressBook.ContactAttribute;

/**
 * Unit Tests for AddressBook.java with Code Coverage of 98%.
 * 
 * @author Nishma Gorwara
 */
public class AddressBookTest {
  private AddressBook addressBookTest;
  private Contact contactTest;

  @Before
  public void setUp() {
    addressBookTest = new AddressBook();
    contactTest = new Contact.Builder().withName("Test Name").withPhoneNumber("1234567890")
        .withEmail("test@test.com")
        .withAddress("Test Street, Apt Test, Test City, Test State, 12345").withNote("Test Note")
        .build();
    addressBookTest.addContact(contactTest);
  }

  @Test
  public void addContactTest() {
    assertTrue("Contact was added", addressBookTest.addContact(contactTest));
  }

  @Test
  public void removeContactTest() {
    assertTrue("Contact was removed", addressBookTest.removeContact(contactTest));
  }

  @Test
  public void searchTest() {
    Contact contactTest1 = new Contact.Builder().withName("Test Name 1")
        .withPhoneNumber("1111111111").withEmail("test1@test.com")
        .withAddress("Test1 Street, Apt Test1, Test City, Test State, 11111")
        .withNote("Test Note 1").build();
    Contact contactTest2 = new Contact.Builder().withName("Test Name 2")
        .withPhoneNumber("2222222222").withEmail("test2@test.com")
        .withAddress("Test2 Street, Apt Test2, Test City, Test State, 22222")
        .withNote("Test Note 2").build();
    Contact contactTest3 = new Contact.Builder().withName("Test Name 3")
        .withPhoneNumber("3333333333").withEmail("test3@test.com")
        .withAddress("Test3 Street, Apt Test3, Test City, Test State, 33333")
        .withNote("Test Note 3").build();

    addressBookTest.addContact(contactTest1);
    addressBookTest.addContact(contactTest2);
    addressBookTest.addContact(contactTest3);

    List<Contact> contactList1 = addressBookTest.search(ContactAttribute.NAME, "test name");
    List<Contact> contactList2 = addressBookTest.search(ContactAttribute.PHONE, "111");
    List<Contact> contactList3 = addressBookTest.search(ContactAttribute.ADDRESS, "22222");
    List<Contact> contactList4 = addressBookTest.search(ContactAttribute.NOTE, "NOTE");
    List<Contact> contactList5 = addressBookTest.search(ContactAttribute.EMAIL, "gmail");

    assertEquals("Contacts returned matching name", contactList1.size(), 4);
    assertEquals("Contacts returned matching phone number", contactList2.size(), 1);
    assertEquals("Contacts returned matching address", contactList3.size(), 1);
    assertEquals("Contacts returned matching note", contactList4.size(), 4);
    assertEquals("Contacts not found given email", contactList5.size(), 0);
  }

  @Test(expected = NullPointerException.class)
  public void searchTest_Null() {
    List<Contact> contactList = addressBookTest.search(ContactAttribute.NAME, null);
  }

  @Test
  public void searchNameTest_Empty() {
    List<Contact> contactList = addressBookTest.search(ContactAttribute.NAME, "");
    assertEquals("All contacts returned", contactList.size(), 1);
  }

  @Test
  public void testSaveRead() {
    String filepath = "file.json";
    try {
      addressBookTest.save(filepath);
      AddressBook addressBookRead = new AddressBook(filepath);
    } catch (Exception e) {
      fail("Exception found in loading/saving data from/to file");
    }
  }

  @Test
  public void toStringTest() {
    String contactString = "Test Name\ntest@test.com\n1234567890\nTest Street, Apt Test, Test City, Test State, 12345\nTest Note\n";
    assertEquals(contactString, addressBookTest.toString());
  }
}
