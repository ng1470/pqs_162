package connectfour;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testConnectFourModel {

  ConnectFourModel testModel;
  ConnectFourView testView;
  ConnectFourBoard testBoard;
  ConnectFourPlayer testHumanPlayer;
  ConnectFourPlayer testComputerPlayer;

  int[][] testBoardGrid;
  int[] testAvailableCells;

  @Before
  public void beforeTests() {
    testModel = new ConnectFourModel();
    testBoard = new ConnectFourBoard();
    testModel.setBoard(testBoard);
    testHumanPlayer = new ConnectFourPlayer.Builder(ConnectFourConstants.PLAYER1)
        .name("Test Human Player").human(true).build();
    testComputerPlayer = new ConnectFourPlayer.Builder(ConnectFourConstants.PLAYER2)
        .name("Test Computer Player").human(false).build();
    testModel.setPlayerOne(testHumanPlayer);
    testModel.setPlayerTwo(testComputerPlayer);
    testAvailableCells = new int[ConnectFourConstants.GRID_COLUMNS];
    testBoardGrid = new int[ConnectFourConstants.GRID_ROWS][ConnectFourConstants.GRID_COLUMNS];
    for (int row = 0; row < ConnectFourConstants.GRID_ROWS; row++) {
      for (int column = 0; column < ConnectFourConstants.GRID_COLUMNS; column++) {
        testBoardGrid[row][column] = -1;
      }
    }
  }

  @After
  public void afterTests() {
    testView = null;
  }

  @Test
  public void testAddListener() {
    testView = new ConnectFourView(testModel);
    assertTrue("Listener not added correctly!", testModel.getListenerList().contains(testView));
  }

  @Test
  public void testGameStart() {
    testModel.startGame();
    assertEquals("Current player not set correctly!", testModel.getPlayerOne(),
        testModel.getCurrentPlayer());
    assertEquals("Total moves not reset correctly!", testModel.getBoard().getTotalMoves(),
        ConnectFourConstants.GRID_ROWS * ConnectFourConstants.GRID_COLUMNS);
    assertArrayEquals("Available cells not reset!", testAvailableCells,
        testModel.getBoard().getAvailableCells());

  }

  @Test
  public void testGameMove() {
    testModel.setcurrentPlayer(testHumanPlayer);
    testModel.gameMove(3);
    assertEquals("Moves are updated correctly!", 1, testHumanPlayer.getMoves());
    assertEquals("Board is not updated correctly", ConnectFourConstants.PLAYER1,
        testModel.getBoard().getBoard()[0][3]);
  }

  @Test
  public void testGameReset() {
    testModel.resetGame();
    assertEquals("Total Moves not reset correctly", testModel.getBoard().getTotalMoves(),
        ConnectFourConstants.GRID_ROWS * ConnectFourConstants.GRID_COLUMNS);
    assertArrayEquals("Available cells not reset", testAvailableCells,
        testModel.getBoard().getAvailableCells());
    int[][] testBoard = testModel.getBoard().getBoard();
    assertArrayEquals("Row 0 not reset correctly", testBoardGrid[0], testBoard[0]);
    assertArrayEquals("Row 1 not reset correctly", testBoardGrid[1], testBoard[1]);
    assertArrayEquals("Row 2 not reset correctly", testBoardGrid[2], testBoard[2]);
    assertArrayEquals("Row 3 not reset correctly", testBoardGrid[3], testBoard[3]);
    assertArrayEquals("Row 4 not reset correctly", testBoardGrid[4], testBoard[4]);
    assertArrayEquals("Row 5 not reset correctly", testBoardGrid[5], testBoard[5]);
  }

}
