package connectfour;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class testConnectFourBoard {

  ConnectFourBoard testBoard;
  int[][] testBoardGrid;
  int[] testAvailableCells;

  @Before
  public void beforeTests() {
    testBoardGrid = new int[ConnectFourConstants.GRID_ROWS][ConnectFourConstants.GRID_COLUMNS];
    testAvailableCells = new int[ConnectFourConstants.GRID_COLUMNS];
    for (int row = 0; row < ConnectFourConstants.GRID_ROWS; row++) {
      for (int column = 0; column < ConnectFourConstants.GRID_COLUMNS; column++) {
        testBoardGrid[row][column] = -1;
      }
    }
    testBoard = new ConnectFourBoard();
    testBoard.setLastPlayer(ConnectFourConstants.PLAYER2);
  }

  @Test
  public void testFindComputerMove_Horizontal() {
    testAvailableCells[0] = 1;
    testAvailableCells[1] = 1;
    testAvailableCells[2] = 1;
    testAvailableCells[3] = 0;
    testBoard.setAvailableCells(testAvailableCells.clone());
    testBoardGrid[0][0] = ConnectFourConstants.PLAYER2;
    testBoardGrid[0][1] = ConnectFourConstants.PLAYER2;
    testBoardGrid[0][2] = ConnectFourConstants.PLAYER2;
    testBoard.setBoard(testBoardGrid.clone());
    assertEquals("Invalid column returned", 3, testBoard.findComputerMove());
  }

  @Test
  public void testFindComputerMove_Vertical() {
    testAvailableCells[3] = 3;
    testBoard.setAvailableCells(testAvailableCells.clone());
    int result = 3;
    testBoardGrid[0][result] = ConnectFourConstants.PLAYER2;
    testBoardGrid[1][result] = ConnectFourConstants.PLAYER2;
    testBoardGrid[2][result] = ConnectFourConstants.PLAYER2;
    testBoard.setBoard(testBoardGrid.clone());
    assertEquals("Invalid column returned", result, testBoard.findComputerMove());
  }

  @Test
  public void testFindComputerMove_LeftDiagonal() {
    testAvailableCells[2] = 3;
    testAvailableCells[3] = 3;
    testAvailableCells[4] = 2;
    testAvailableCells[5] = 1;
    testBoard.setAvailableCells(testAvailableCells.clone());
    testBoardGrid[0][5] = ConnectFourConstants.PLAYER2;
    testBoardGrid[1][4] = ConnectFourConstants.PLAYER2;
    testBoardGrid[2][3] = ConnectFourConstants.PLAYER2;
    testBoard.setBoard(testBoardGrid.clone());
    assertEquals("Invalid column returned", 2, testBoard.findComputerMove());
  }

  @Test
  public void testFindComputerMove_RightDiagonal() {
    testAvailableCells[0] = 1;
    testAvailableCells[1] = 2;
    testAvailableCells[2] = 3;
    testAvailableCells[3] = 3;
    testBoard.setAvailableCells(testAvailableCells.clone());
    testBoardGrid[0][0] = ConnectFourConstants.PLAYER2;
    testBoardGrid[1][1] = ConnectFourConstants.PLAYER2;
    testBoardGrid[2][2] = ConnectFourConstants.PLAYER2;
    testBoard.setBoard(testBoardGrid.clone());
    assertEquals("Invalid column returned", 3, testBoard.findComputerMove());
  }

  @Test
  public void testFindWinner_Horizontal() {
    int testRow = 2;
    int testColumn = 3;
    testAvailableCells[testColumn] = testRow;
    testBoard.setAvailableCells(testAvailableCells.clone());
    testBoardGrid[testRow][0] = ConnectFourConstants.PLAYER2;
    testBoardGrid[testRow][1] = ConnectFourConstants.PLAYER2;
    testBoardGrid[testRow][2] = ConnectFourConstants.PLAYER2;
    testBoardGrid[testRow][3] = ConnectFourConstants.PLAYER2;
    testBoard.setBoard(testBoardGrid.clone());
    testBoard.setLastPlayedRow(testRow);
    testBoard.setLastPlayedColumn(testColumn);
    assertTrue("Invalid value for winner!", testBoard.findWinner());
  }

  @Test
  public void testFindWinner_Vertical() {
    int testRow = 3;
    int testColumn = 2;
    testAvailableCells[testRow] = testColumn;
    testBoard.setAvailableCells(testAvailableCells.clone());
    testBoardGrid[0][testColumn] = ConnectFourConstants.PLAYER2;
    testBoardGrid[1][testColumn] = ConnectFourConstants.PLAYER2;
    testBoardGrid[2][testColumn] = ConnectFourConstants.PLAYER2;
    testBoardGrid[3][testColumn] = ConnectFourConstants.PLAYER2;
    testBoard.setBoard(testBoardGrid.clone());
    testBoard.setLastPlayedRow(testRow);
    testBoard.setLastPlayedColumn(testColumn);
    assertTrue("Invalid value for winner!", testBoard.findWinner());
  }

  @Test
  public void testFindWinner_LeftDiagonal() {
    testAvailableCells[3] = 4;
    testBoard.setAvailableCells(testAvailableCells.clone());
    testBoardGrid[3][3] = ConnectFourConstants.PLAYER2;
    testBoardGrid[2][4] = ConnectFourConstants.PLAYER2;
    testBoardGrid[4][2] = ConnectFourConstants.PLAYER2;
    testBoardGrid[5][1] = ConnectFourConstants.PLAYER2;
    testBoard.setBoard(testBoardGrid.clone());
    testBoard.setLastPlayedRow(3);
    testBoard.setLastPlayedColumn(3);
    assertTrue("Invalid value for winner!", testBoard.findWinner());
  }

  @Test
  public void testFindWinner_RightDiagonal() {
    testAvailableCells[0] = 0;
    testBoard.setAvailableCells(testAvailableCells.clone());
    testBoardGrid[0][0] = ConnectFourConstants.PLAYER2;
    testBoardGrid[1][1] = ConnectFourConstants.PLAYER2;
    testBoardGrid[2][2] = ConnectFourConstants.PLAYER2;
    testBoardGrid[3][3] = ConnectFourConstants.PLAYER2;
    testBoard.setBoard(testBoardGrid.clone());
    testBoard.setLastPlayedRow(0);
    testBoard.setLastPlayedColumn(0);
    assertTrue("Invalid value for winner!", testBoard.findWinner());
  }

  @Test
  public void testUpdateBoard() {
    int testRow = 2;
    int testColumn = 1;
    int testMoves = 14;
    testAvailableCells[testColumn] = testRow;
    testBoard.setAvailableCells(testAvailableCells.clone());
    testBoard.setTotalMoves(testMoves);
    int row = testBoard.updateBoard(testColumn, ConnectFourConstants.PLAYER1);
    assertEquals("Game board not updated correctly!", testBoard.getBoard()[testRow][testColumn],
        ConnectFourConstants.PLAYER1);
    assertEquals("Total moves not updated correctly!", testBoard.getTotalMoves(), testMoves - 1);
    assertEquals("Available cells not updated correctly!",
        testBoard.getAvailableCells()[testColumn], testAvailableCells[testColumn] + 1);
    assertEquals("Invalid row returned!", row, testRow);
  }

  @Test
  public void testUpdateBoard_ColumnFull() {
    int testColumn = 4;
    int testRow = ConnectFourConstants.GRID_ROWS;
    testAvailableCells[testColumn] = testRow;
    testBoard.setAvailableCells(testAvailableCells.clone());
    int row = testBoard.updateBoard(testColumn, ConnectFourConstants.PLAYER1);
    assertEquals("Invalid row returned!", row, -1);
  }

  @Test
  public void testResetBoard() {
    testBoard.resetBoard();
    assertEquals("Total moves not reset correctly!", testBoard.getTotalMoves(),
        ConnectFourConstants.GRID_ROWS * ConnectFourConstants.GRID_COLUMNS);
    assertArrayEquals("Available cells not reset!", testAvailableCells,
        testBoard.getAvailableCells());
    int[][] gameBoardGrid = testBoard.getBoard();
    assertArrayEquals("Row 0 not reset correctly!", testBoardGrid[0], gameBoardGrid[0]);
    assertArrayEquals("Row 1 not reset correctly!", testBoardGrid[1], gameBoardGrid[1]);
    assertArrayEquals("Row 2 not reset correctly!", testBoardGrid[2], gameBoardGrid[2]);
    assertArrayEquals("Row 3 not reset correctly!", testBoardGrid[3], gameBoardGrid[3]);
    assertArrayEquals("Row 4 not reset correctly!", testBoardGrid[4], gameBoardGrid[4]);
    assertArrayEquals("Row 5 not reset correctly!", testBoardGrid[5], gameBoardGrid[5]);
  }

}
