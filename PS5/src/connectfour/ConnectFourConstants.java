package connectfour;

import java.awt.Color;

/**
 * Class contains all the constants required in the Connect Four board game.
 * 
 * @author Nishma
 *
 */

public class ConnectFourConstants {
  //Number of rows and columns in the game grid.
  public static final int GRID_ROWS = 6;
  public static final int GRID_COLUMNS = 7;

  //Player ID for each of the two game players.
  public static final int PLAYER1 = 1;
  public static final int PLAYER2 = 2;

  //Colors to denote the blocks of the two game players.
  public static final Color COLOR_PLAYER1 = Color.BLUE;
  public static final Color COLOR_PLAYER2 = Color.RED;

  //Number of blocks required in a sequence to win.
  public static final int CONNECT_FOUR = 4;

  //Mode for players in the game.
  public static final int HUMAN_HUMAN = 1;
  public static final int COMPUTER_HUMAN = 2;
 
  //Values for action of the buttons.
  public static final int ACTION_START = 1;
  public static final int ACTION_NEW = 2;
}
