package connectfour;

/**
 * Class uses the builder pattern. It represents the players in the Connect Four
 * board game which can be human or computer.
 *
 * @author Nishma
 *
 */

public class ConnectFourPlayer {

  private int id;
  private String name;
  private int moves;
  private boolean human;

  public static class Builder {

    private int id;
    private String name;
    private int moves = 0;
    private boolean human;

    public Builder(int id) {
      this.id = id;
    }

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder moves(int moves) {
      this.moves = moves;
      return this;
    }

    public Builder human(Boolean human) {
      this.human = human;
      return this;
    }

    public ConnectFourPlayer build() {
      return new ConnectFourPlayer(this);
    }
  }

  private ConnectFourPlayer(Builder builder) {
    this.id = builder.id;
    this.name = builder.name;
    this.moves = builder.moves;
    this.human = builder.human;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getMoves() {
    return moves;
  }

  public boolean isHuman() {
    return human;
  }

  public void incrementMoves() {
    moves = moves + 1;
  }

  @Override
  public boolean equals(Object object) {
    if (object == null) {
      return false;
    }
    if (getClass() != object.getClass()) {
      return false;
    }
    if (this == object) {
      return true;
    }
    ConnectFourPlayer player = (ConnectFourPlayer) object;
    if (human != player.human) {
      return false;
    }
    if (id != player.id) {
      return false;
    }
    if (name == null) {
      if (player.name != null) {
        return false;
      }
    } else if (!name.equals(player.name)) {
      return false;
    }
    if (moves != player.moves) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 13;
    int result = 1;
    result = prime * result + id;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + moves;
    result = prime * result + (human ? 1213 : 1215);
    return result;
  }

  @Override
  public String toString() {
    return "ConnectFourPlayer [id = " + id + ", name = " + name + ", moves = " + moves
        + ", human = " + human + "]";
  }

}
