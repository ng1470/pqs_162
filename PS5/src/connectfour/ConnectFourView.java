package connectfour;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * View class for observer pattern implemented for Connect Four board game.
 * 
 * @author Nishma
 *
 */
public class ConnectFourView implements ConnectFourListener {

  private JFrame mainFrame;
  private JPanel mainPanel;
  private JPanel centerPanel;
  private JButton[][] boardButtons;
  private JButton startButton;
  private JPanel boardPanel = new JPanel(
      new GridLayout(ConnectFourConstants.GRID_ROWS, ConnectFourConstants.GRID_COLUMNS));
  private JRadioButton computerHuman;
  private JRadioButton humanHuman;

  private ConnectFourModel model;
  private ConnectFourPlayer playerOne;
  private ConnectFourPlayer playerTwo;
  private int gameMode = ConnectFourConstants.HUMAN_HUMAN;
  private boolean gameActive = false;

  public ConnectFourView(ConnectFourModel model) {
    this.model = model;
    model.addListener(this);
    setBoardButtons();
    setMainFrame();
    setPlayers();
  }

  private void setMainFrame() {
    mainFrame = new JFrame("Connect Four Board Game");
    mainPanel = new JPanel(new BorderLayout());
    mainPanel.add(getTopPanel(), BorderLayout.NORTH);
    centerPanel = getCenterPanel();
    mainPanel.add(centerPanel, BorderLayout.CENTER);
    mainFrame.setSize(1000, 1000);
    mainFrame.setResizable(false);
    mainFrame.setVisible(true);
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    mainFrame.getContentPane().add(mainPanel);
  }

  @Override
  public void gameStart() {
    for (JButton[] rowButtons : boardButtons) {
      for (JButton cellButton : rowButtons)
        cellButton.setText("");
    }
  }

  @Override
  public void makeMove(int playerId, int row, int column) {
    if (playerId == ConnectFourConstants.PLAYER1) {
      boardButtons[row][column].setBackground(ConnectFourConstants.COLOR_PLAYER1);
    } else {
      boardButtons[row][column].setBackground(ConnectFourConstants.COLOR_PLAYER2);
    }
  }

  @Override
  public void wrongMove(String message) {
    JOptionPane.showMessageDialog(mainFrame, message, "Move not allowed!",
        JOptionPane.ERROR_MESSAGE);
  }

  @Override
  public void gameWinner(String message) {
    JOptionPane.showMessageDialog(mainFrame, message, "Game won!", JOptionPane.PLAIN_MESSAGE);
    gameReset();
  }

  @Override
  public void gameOver() {
    JOptionPane.showMessageDialog(mainFrame, "Game Over!", "Game Over!", JOptionPane.ERROR_MESSAGE);
    gameReset();
  }

  @Override
  public void gameReset() {
    gameActive = false;
    startButton.setText("Start");
    startButton.putClientProperty("action", ConnectFourConstants.ACTION_START);

    boardPanel.removeAll();
    boardPanel.revalidate();
    setBoardButtons();
    getCenterPanel();

    gameMode = ConnectFourConstants.HUMAN_HUMAN;
    humanHuman.setEnabled(true);
    humanHuman.setSelected(true);
    computerHuman.setEnabled(true);
    computerHuman.setSelected(false);
    setPlayers();
  }

  private JPanel getCenterPanel() {
    for (int row = ConnectFourConstants.GRID_ROWS - 1; row >= 0; row--) {
      for (int column = 0; column < ConnectFourConstants.GRID_COLUMNS; column++) {
        boardPanel.add(boardButtons[row][column]);
      }
    }
    return boardPanel;
  }

  private JPanel getTopPanel() {
    JPanel topPanel = new JPanel(new FlowLayout());

    humanHuman = new JRadioButton("Human vs Human");
    humanHuman.setMnemonic(KeyEvent.VK_H);
    humanHuman.putClientProperty("value", ConnectFourConstants.HUMAN_HUMAN);
    humanHuman.addActionListener(new PlayerActionListener());
    humanHuman.setSelected(true);

    computerHuman = new JRadioButton("Computer vs Human");
    computerHuman.setMnemonic(KeyEvent.VK_C);
    computerHuman.putClientProperty("value", ConnectFourConstants.COMPUTER_HUMAN);
    computerHuman.addActionListener(new PlayerActionListener());

    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(humanHuman);
    buttonGroup.add(computerHuman);

    startButton = new JButton("Start");
    startButton.putClientProperty("action", ConnectFourConstants.ACTION_START);
    startButton.addActionListener(new StartButtonActionListener());

    topPanel.add(humanHuman);
    topPanel.add(computerHuman);
    topPanel.add(startButton);

    return topPanel;
  }

  private void setBoardButtons() {
    boardButtons = new JButton[ConnectFourConstants.GRID_ROWS][ConnectFourConstants.GRID_COLUMNS];
    for (int row = ConnectFourConstants.GRID_ROWS - 1; row >= 0; row--) {
      for (int column = 0; column < ConnectFourConstants.GRID_COLUMNS; column++) {
        boardButtons[row][column] = new JButton("");
        boardButtons[row][column].putClientProperty("column", column);
        boardButtons[row][column].putClientProperty("row", row);
        boardButtons[row][column].addActionListener(new BoardButtonActionListener());
      }
    }
  }

  private void setPlayers() {
    if (gameMode == ConnectFourConstants.HUMAN_HUMAN) {
      playerOne = new ConnectFourPlayer.Builder(ConnectFourConstants.PLAYER1).name("Player1")
          .human(true).build();
      playerTwo = new ConnectFourPlayer.Builder(ConnectFourConstants.PLAYER2).name("Player2")
          .human(true).build();
    } else {
      playerOne = new ConnectFourPlayer.Builder(ConnectFourConstants.PLAYER1).name("Human")
          .human(true).build();
      playerTwo = new ConnectFourPlayer.Builder(ConnectFourConstants.PLAYER2).name("Computer")
          .human(false).build();
    }
    model.setPlayerOne(playerOne);
    model.setPlayerTwo(playerTwo);
  }

  private class BoardButtonActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent event) {
      if (gameActive) {
        JButton button = (JButton) event.getSource();
        int column = (int) button.getClientProperty("column");
        model.gameMove(column);
      } else {
        JOptionPane.showMessageDialog(mainFrame, "Wait for your turn or Click start button",
            "Wrong move", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  private class StartButtonActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent event) {
      if (gameMode == -1) {
        JOptionPane.showMessageDialog(mainFrame, "Choose a game mode", "Invalid game mode!",
            JOptionPane.ERROR_MESSAGE);
      } else {
        gameActive = true;
        int action = (int) startButton.getClientProperty("action");
        if (action == ConnectFourConstants.ACTION_START) {
          humanHuman.setEnabled(false);
          computerHuman.setEnabled(false);
          startButton.setText("New Game");
          startButton.putClientProperty("action", ConnectFourConstants.ACTION_NEW);
          model.startGame();
        } else {
          gameReset();
        }
      }
    }
  }

  private class PlayerActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent event) {
      AbstractButton abstractButton = (AbstractButton) event.getSource();
      gameMode = (int) abstractButton.getClientProperty("value");
      setPlayers();
    }
  }

}
