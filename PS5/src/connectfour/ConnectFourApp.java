package connectfour;

/**
 * Main class to launch the Connect Four board game as a Java application. The
 * game is played by two players who alternate dropping chips into a 7-column,
 * 6-row grid. The first player to get 4 in a row wins.
 * 
 * There are 2 modes in the game. One, support for 2-players playing at the same
 * machine. Second, support for a computer opponent.
 * 
 * 
 * @author Nishma
 *
 */

public class ConnectFourApp {

  /**
   * Method to initialize the view and model objects of the observer pattern
   * implemented for the Connect Four board game.
   *
   */
  private void init() {
    ConnectFourModel model = new ConnectFourModel();
    new ConnectFourView(model);
  }

  public static void main(String args[]) {
    new ConnectFourApp().init();
  }

}
