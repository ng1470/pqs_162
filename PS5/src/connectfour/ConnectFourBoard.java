package connectfour;

/**
 * Class to maintain the board required to play the Connect Four game.
 * 
 * @author Nishma
 *
 */

public class ConnectFourBoard {

  private int[][] board = new int[ConnectFourConstants.GRID_ROWS][ConnectFourConstants.GRID_COLUMNS];
  private int lastPlayedRow;
  private int lastPlayedColumn;
  private int lastPlayer;
  private int totalMoves = ConnectFourConstants.GRID_ROWS * ConnectFourConstants.GRID_COLUMNS;
  private int[] availableCells = new int[ConnectFourConstants.GRID_COLUMNS];

  public int[][] getBoard() {
    return board.clone();
  }

  public int getLastPlayedRow() {
    return lastPlayedRow;
  }

  public int getLastPlayedColumn() {
    return lastPlayedColumn;
  }

  public int getLastPlayer() {
    return lastPlayer;
  }

  public int[] getAvailableCells() {
    return availableCells;
  }

  public int getTotalMoves() {
    return totalMoves;
  }

  public void setBoard(int[][] board) {
    this.board = board;
  }

  public void setLastPlayedRow(int lastPlayedRow) {
    this.lastPlayedRow = lastPlayedRow;
  }

  public void setLastPlayedColumn(int lastPlayedColumn) {
    this.lastPlayedColumn = lastPlayedColumn;
  }

  public void setLastPlayer(int lastPlayer) {
    this.lastPlayer = lastPlayer;
  }

  public void setTotalMoves(int totalMoves) {
    this.totalMoves = totalMoves;
  }

  public void setAvailableCells(int[] availableCells) {
    this.availableCells = availableCells;
  }

  private int findHorizontalLength(int column, int row) {
    int rowIndex = row;
    int columnIndex = column;
    int length = 0;
    while (columnIndex < ConnectFourConstants.GRID_COLUMNS) {
      if (board[rowIndex][columnIndex] != lastPlayer) {
        break;
      }
      columnIndex++;
      length++;
    }
    columnIndex = column - 1;
    while (columnIndex > -1) {
      if (board[rowIndex][columnIndex] != lastPlayer) {
        break;
      }
      columnIndex--;
      length++;
    }
    return length;
  }

  private int findVerticalLength(int column, int row) {
    int rowIndex = row;
    int columnIndex = column;
    int length = 0;
    while (rowIndex < ConnectFourConstants.GRID_ROWS) {
      if (board[rowIndex][columnIndex] != lastPlayer) {
        break;
      }
      rowIndex++;
      length++;
    }
    rowIndex = row - 1;
    while (rowIndex > -1) {
      if (board[rowIndex][columnIndex] != lastPlayer) {
        break;
      }
      rowIndex--;
      length++;
    }
    return length;
  }

  private int findLeftDiagonalLength(int column, int row) {
    int rowIndex = row;
    int columnIndex = column;
    int length = 0;
    while (rowIndex < ConnectFourConstants.GRID_ROWS && columnIndex > -1) {
      if (board[rowIndex][columnIndex] != lastPlayer) {
        break;
      }
      rowIndex++;
      columnIndex--;
      length++;
    }
    rowIndex = row - 1;
    columnIndex = column + 1;
    while (rowIndex > -1 && columnIndex < ConnectFourConstants.GRID_COLUMNS) {
      if (board[rowIndex][columnIndex] != lastPlayer) {
        break;
      }
      rowIndex--;
      columnIndex++;
      length++;
    }
    return length;
  }

  private int findRightDiagonalLength(int row, int column) {
    int rowIndex = row;
    int columnIndex = column;
    int length = 0;
    while (rowIndex < ConnectFourConstants.GRID_ROWS
        && columnIndex < ConnectFourConstants.GRID_COLUMNS) {
      if (board[rowIndex][columnIndex] != lastPlayer) {
        break;
      }
      rowIndex++;
      columnIndex++;
      length++;
    }
    rowIndex = row - 1;
    columnIndex = column - 1;
    while (rowIndex > -1 && columnIndex > -1) {
      if (board[rowIndex][columnIndex] != lastPlayer) {
        break;
      }
      rowIndex--;
      columnIndex--;
      length++;
    }
    return length;
  }

  public int findComputerMove() {
    int columnIndex = -1;
    int length = 0;

    for (int column = 0; column < ConnectFourConstants.GRID_COLUMNS; column++) {
      for (int row = availableCells[column]; row < ConnectFourConstants.GRID_ROWS; row++) {
        int horizontalLength = findHorizontalLength(column, row);
        if (horizontalLength > length) {
          columnIndex = column;
          length = horizontalLength;
        }
        int verticalLength = findVerticalLength(column, row);
        if (verticalLength > length) {
          columnIndex = column;
          length = verticalLength;
        }
        int leftdiagonalLength = findLeftDiagonalLength(column, row);
        if (leftdiagonalLength > length) {
          columnIndex = column;
          length = leftdiagonalLength;
        }
        int rightdiagonalLength = findRightDiagonalLength(column, row);
        if (rightdiagonalLength > length) {
          columnIndex = column;
          length = rightdiagonalLength;
        }
      }
    }
    return columnIndex;
  }

  public boolean findWinner() {
    if (findHorizontalLength(lastPlayedColumn,
        lastPlayedRow) >= ConnectFourConstants.CONNECT_FOUR) {
      return true;
    }
    if (findVerticalLength(lastPlayedColumn, lastPlayedRow) >= ConnectFourConstants.CONNECT_FOUR) {
      return true;
    }
    if (findLeftDiagonalLength(lastPlayedColumn,
        lastPlayedRow) >= ConnectFourConstants.CONNECT_FOUR) {
      return true;
    }
    if (findRightDiagonalLength(lastPlayedColumn,
        lastPlayedRow) >= ConnectFourConstants.CONNECT_FOUR) {
      return true;
    }
    return false;
  }

  public int updateBoard(int column, int playerId) {
    int row = availableCells[column];
    if (row == ConnectFourConstants.GRID_ROWS) {
      return -1;
    }
    board[row][column] = playerId;
    availableCells[column]++;
    totalMoves--;
    return row;
  }

  public void resetBoard() {
    for (int row = 0; row < ConnectFourConstants.GRID_ROWS; row++) {
      for (int column = 0; column < ConnectFourConstants.GRID_COLUMNS; column++) {
        board[row][column] = -1;
      }
    }
    availableCells = new int[ConnectFourConstants.GRID_COLUMNS];
    totalMoves = ConnectFourConstants.GRID_ROWS * ConnectFourConstants.GRID_COLUMNS;
  }
}
