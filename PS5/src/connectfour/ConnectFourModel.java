package connectfour;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Class to maintain the logic required to play the Connect Four board game.
 * 
 * @author Nishma
 *
 */

public class ConnectFourModel {

  private ConnectFourPlayer playerOne;
  private ConnectFourPlayer playerTwo;
  private ConnectFourPlayer currentPlayer;
  private ConnectFourBoard board = new ConnectFourBoard();
  private List<ConnectFourListener> listenerList = new ArrayList<ConnectFourListener>();

  public ConnectFourPlayer getPlayerOne() {
    return playerOne;
  }

  public ConnectFourPlayer getPlayerTwo() {
    return playerTwo;
  }

  public ConnectFourPlayer getCurrentPlayer() {
    return currentPlayer;
  }

  public ConnectFourBoard getBoard() {
    return board;
  }

  public List<ConnectFourListener> getListenerList() {
    return Collections.unmodifiableList(listenerList);
  }

  public void setPlayerOne(ConnectFourPlayer playerOne) {
    this.playerOne = playerOne;
  }

  public void setPlayerTwo(ConnectFourPlayer playerTwo) {
    this.playerTwo = playerTwo;
  }

  public void setcurrentPlayer(ConnectFourPlayer currentPlayer) {
    this.currentPlayer = currentPlayer;
  }

  public void setBoard(ConnectFourBoard board) {
    this.board = board;
  }

  public void setListenerList(List<ConnectFourListener> listenerList) {
    this.listenerList = listenerList;
  }

  public void addListener(ConnectFourListener listener) {
    if (listener instanceof ConnectFourListener) {
      listenerList.add(listener);
    }
  }

  public void startGame() {
    board.resetBoard();
    this.fireGameStartEvent();
    currentPlayer = playerOne;
  }

  private void fireGameStartEvent() {
    for (ConnectFourListener listener : listenerList) {
      listener.gameStart();
    }
  }

  private void makePlayerMove(int column) {
    int row = board.updateBoard(column, currentPlayer.getId());
    if (row == -1) {
      fireWrongMoveEvent("Choose another column as this column is already full.");
    } else {
      fireMakeMoveEvent(currentPlayer.getId(), row, column);
      board.setLastPlayer(currentPlayer.getId());
      currentPlayer.incrementMoves();
      board.setLastPlayedRow(row);
      board.setLastPlayedColumn(column);
      if (currentPlayer.getMoves() >= ConnectFourConstants.CONNECT_FOUR && board.findWinner()) {
        fireGameWinnerEvent(currentPlayer.getName() + " is the winner!");
      } else if (board.getTotalMoves() == 0) {
        fireGameOverEvent();
      }
      if (currentPlayer.equals(playerOne)) {
        currentPlayer = playerTwo;
      } else {
        currentPlayer = playerOne;
      }
    }
  }

  private void fireWrongMoveEvent(String message) {
    for (ConnectFourListener listener : listenerList) {
      listener.wrongMove(message);
    }
  }

  private void fireMakeMoveEvent(int playerId, int row, int column) {
    for (ConnectFourListener listener : listenerList) {
      listener.makeMove(playerId, row, column);
    }
  }

  private void fireGameWinnerEvent(String message) {
    for (ConnectFourListener listener : listenerList) {
      listener.gameWinner(message);
    }
  }

  private void fireGameOverEvent() {
    for (ConnectFourListener listener : listenerList) {
      listener.gameOver();
    }
  }

  private void makeComputerMove() {
    board.setLastPlayer(currentPlayer.getId());
    if (currentPlayer.getMoves() == 0) {
      Random random = new Random();
      int randomNumber = random.nextInt(ConnectFourConstants.GRID_COLUMNS);
      makePlayerMove(randomNumber);
    } else {
      int column = board.findComputerMove();
      makePlayerMove(column);
    }
  }

  public void gameMove(int column) {
    makePlayerMove(column);
    if (!currentPlayer.isHuman()) {
      makeComputerMove();
    }
  }

  public void resetGame() {
    board.resetBoard();
    fireGameResetEvent();
  }

  private void fireGameResetEvent() {
    for (ConnectFourListener listener : listenerList) {
      listener.gameReset();
    }
  }
}
