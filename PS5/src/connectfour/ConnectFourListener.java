package connectfour;

/**
 * Listener interface for the observer pattern implemented for the Connect Four
 * game.
 * 
 * @author Nishma
 *
 */

public interface ConnectFourListener {

  /**
   * Method to start the game by enabling components on the board for players.
   *
   */
  public void gameStart();

  /**
   * Method to make a move in the game by selecting a column to drop a chip.
   * 
   * @param playerId
   *          Id of the player who made the last move.
   * 
   * @param row
   *          Row number where last move was made.
   * 
   * @param column
   *          Column number where the last move was made.
   *
   */
  public void makeMove(int playerId, int row, int column);

  /**
   * Method to alert the players for wrong or invalid move made.
   * 
   * @param message
   *          Message to display in the alert box.
   *
   */
  public void wrongMove(String message);

  /**
   * Method to declare the winner of th e current game.
   *
   * @param message
   *          Message to display in the alert box.
   */
  public void gameWinner(String message);

  /**
   * Method to end the game if the total moves are done.
   *
   */
  public void gameOver();

  /**
   * Method to reset the game to the initial state.
   *
   */
  public void gameReset();

}
