package edu.nyu.pqs.stopwatch.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import edu.nyu.pqs.stopwatch.api.Stopwatch;

/**
 * This is a class implementing Stopwatch interface and its methods. An instance
 * of the class can be created by passing a Stopwatch ID.
 * 
 * @author Nishma
 */
public class MyImplementation implements Stopwatch {

  /**
   * Various states of stopwatch are represented by enum. 
   * START: Stopwatch is reset.
   * STOP: Stopwatch is stopped.
   * RUN: Stopwatch is running.
   */
  public static enum StopwatchState {
    START, STOP, RUN;
  }

  private String stopwatchID;
  private StopwatchState stopwatchState;
  private long finalLapTime = 0;
  private List<Long> lapTimeList = new ArrayList<Long>();
  private Object objectLock = new Object();

  /**
   * Instance of the class is created by passing a stopwatch ID.
   * 
   * @param stopwatchID
   *            The identifier of new stopwatch.
   */
  public MyImplementation(String stopwatchID) {
    this.stopwatchID = stopwatchID;
    stopwatchState = StopwatchState.START;
  }

  /**
   * Method returns the current system time in milliseconds.
   * 
   * @return system time
   */
  private long getSystemTime() {
    return System.currentTimeMillis();
  }

  /**
   * Method calculates total lap time of the stopwatch.
   * 
   * @return totalTime
   */
  private long totalLapTime() {
    long totalTime = 0;
    for (Long lapTime : lapTimeList) {
      totalTime += lapTime;
    }
    return totalTime;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(stopwatchID.hashCode());
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Stopwatch)) {
      return false;
    } else {
      Stopwatch stopwatch = (Stopwatch) obj;
      return this.getId().equals(stopwatch.getId());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.nyu.pqs.stopwatch.api.Stopwatch#getId()
   */
  @Override
  public String getId() {
    return stopwatchID;
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.nyu.pqs.stopwatch.api.Stopwatch#start()
   */
  @Override
  public void start() {
    synchronized (objectLock) {
      if (stopwatchState == StopwatchState.RUN) {
        throw new IllegalStateException("Stopwatch is already running!");
      } else if (stopwatchState == StopwatchState.START) {
        finalLapTime = getSystemTime();
      }
      stopwatchState = StopwatchState.RUN;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.nyu.pqs.stopwatch.api.Stopwatch#lap()
   */
  @Override
  public void lap() {
    synchronized (objectLock) {
      if (stopwatchState != StopwatchState.RUN) {
        throw new IllegalStateException("Stopwatch is not running!");
      }
      lapTimeList.add(getSystemTime() - totalLapTime() - finalLapTime);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.nyu.pqs.stopwatch.api.Stopwatch#stop()
   */
  @Override
  public void stop() {
    synchronized (objectLock) {
      lap();
      stopwatchState = StopwatchState.STOP;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.nyu.pqs.stopwatch.api.Stopwatch#reset()
   */
  @Override
  public void reset() {
    synchronized (objectLock) {
      if (stopwatchState == StopwatchState.RUN) {
        stop();
      }
      lapTimeList.clear();
      stopwatchState = StopwatchState.START;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see edu.nyu.pqs.stopwatch.api.Stopwatch#getLapTimes()
   */
  @Override
  public List<Long> getLapTimes() {
    synchronized (objectLock) {
      return lapTimeList;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "MyImplementation [stopwatchID=" + stopwatchID + "]";
  }
}
